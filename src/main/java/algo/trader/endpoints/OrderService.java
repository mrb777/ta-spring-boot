package algo.trader.endpoints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import algo.trader.domain.Currency;
import algo.trader.domain.MarketOrder;
import algo.trader.domain.Side;

@RestController
public class OrderService {
    private List<MarketOrder> orders = new ArrayList<>();

    // C
    @PostMapping
    public int addOrder(@RequestBody MarketOrder order) {
        orders.add(order);

        return orders.size() - 1;
    }

    // R
    @GetMapping
    public List<MarketOrder> getOrders() {
        return orders;
    }

    @GetMapping(value = "/{orderId}")
    public MarketOrder getOrder(@PathVariable int orderId) {
        System.out.println("retrieving order " + orderId);
        return orders.get(orderId);
    }

    // U
    @PostMapping(value = "/{orderId}")
    public void setOrder(@PathVariable int orderId, @RequestBody MarketOrder order) {
        orders.set(orderId, order);
    }

    // D
    @PostMapping(value = "/{orderId}")
    public void deleteOrder(@PathVariable int orderId) {
        orders.remove(orderId);
    }
}
