package algo.trader.endpoints;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CurrencyService {
    private List<String> newCurrencies = new ArrayList<>();

    // C
    @PostMapping
    public void addCurrency(@RequestBody String currency) {
        newCurrencies.add(currency);
    }

    // R
    @GetMapping
    public List<String> getCurrencies() {
        return newCurrencies;
    }

    // U
    @PutMapping(value="/{currencyId}")
    public void setCurrency(@PathVariable int currencyId, @RequestBody String currency) {
        newCurrencies.set(currencyId, currency);
    }

    // D
    @DeleteMapping(value="/{currencyId}")
    public void deleteCurrency(@PathVariable int currencyId) {
        newCurrencies.remove(currencyId);
    }
}
