package algo.trader.endpoints;

import algo.trader.domain.Currency;
import algo.trader.domain.MarketOrder;
import algo.trader.domain.Side;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by balu on 30.07.2017.
 */
public class OrderServiceTest {

    OrderService orderService;

    @Before
    public void prepare() {
        orderService = new OrderService();
    }

    @Test
    public void addOrderTest() throws Exception {
        // When
        orderService.addOrder(new MarketOrder(Currency.EUR, 10.0, Side.BUY));
        //Then
        assertEquals(orderService.getOrders().size(), 1);

        assertEquals(orderService.getOrder(0).getCurrency(), Currency.EUR);
        assertEquals(orderService.getOrder(0).getAmount(), 10.0, 0.1);
        assertEquals(orderService.getOrder(0).getSide(), Side.BUY);
    }

    @Test
    public void getOrderTest() throws Exception {
        // When
        orderService.addOrder(new MarketOrder(Currency.GBP, 20.0, Side.SELL));
        orderService.addOrder(new MarketOrder(Currency.EUR, 10.0, Side.BUY));

        // Then
        assertEquals(orderService.getOrder(0).getCurrency(), Currency.GBP);
        assertEquals(orderService.getOrder(0).getAmount(), 20.0, 0.1);
        assertEquals(orderService.getOrder(0).getSide(), Side.SELL);
    }

    @Test
    public void setOrderTest() throws Exception {
        // When
        orderService.addOrder(new MarketOrder(Currency.GBP, 20.0, Side.SELL));
        orderService.setOrder(0, new MarketOrder(Currency.EUR, 10.0, Side.BUY));

        // Then
        assertEquals(orderService.getOrder(0).getCurrency(), Currency.EUR);
        assertEquals(orderService.getOrder(0).getAmount(), 10.0, 0.1);
        assertEquals(orderService.getOrder(0).getSide(), Side.BUY);
    }

    @Test
    public void deleteOrderTest() throws Exception {
        // When
        orderService.addOrder(new MarketOrder(Currency.EUR, 10.0, Side.BUY));
        orderService.deleteOrder(0);

        // Then
        assertEquals(orderService.getOrders().size(), 0);
    }


    // Then
    @Test(expected = IndexOutOfBoundsException.class)
    public void deleteOrderTestException() throws Exception {

        // When
        orderService.deleteOrder(0);
    }

    // Then
    @Test(expected = IndexOutOfBoundsException.class)
    public void getOrderTestException() throws Exception {

        // When
        orderService.getOrder(0);
    }
}